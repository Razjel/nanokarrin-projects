Polski cover True Damage - GIANTS (GIGANCI).

Słowo od reżysera:
Szczęśliwego Nowego Roku! Chciałbym podziękować najmocniej wszystkim z ekipy, bo bez nich nie dałbym rady tego wydać!

--

EKIPA PROJEKTU: 
Reżyseria: Spidek
Tekst piosenki: Rose
Realizacja dźwięku: Spidek
Tuning: Yarkin, Baquu
Edycja obrazu: Kapsel
Wokal: Rose, Baquu, Kara, Yarkin, Spidek
#TrueDamage #Giants #Polish
--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Rose
Wersja NanoKarrin: @Fandubbing

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: (True Damage - GIANTS)
Więcej o projekcie: http://nanokarrin.pl/
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal:    http://nanokarrin.pl
//Discord:    http://discord.gg/nanokarrin
//Facebook:    http://fb.com/nanokarrin
//Instagram:    #nanokarrin

--

DISCLAIMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support  its rightful owners!