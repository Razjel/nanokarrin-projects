Polski cover piosenki z gry God Eater - Over the Clouds.

A Pchełka na to…
Ten film zaczyna całą serię moich “starszych” projektów, które wreszcie ujrzą światło dzienne. Over the Clouds powstawał przez ponad trzy lata i jest o tyle wyjątkowy, że zawiera ukryty feature (o ile można go tak nazwać), dlatego też wydajemy go właśnie 3 marca. Jestem ciekawa, ile z Was go zauważy (biegnijcie sprawdzać kalendarze!).
Wielkie brawa dla ekipy, zwłaszcza mojego niezastąpionego koreżysera, Kary, bez Ciebie by nie wyszło.
UDAŁO SIĘ!

--

EKIPA PROJEKTU: 
Reżyseria: Pchełka
Koreżyseria: Kara
Tekst piosenki: Pchełka
Realizacja dźwięku: Rzulia
Edycja obrazu: Kara
Wokal: Shiguroya
Ilustracje (szkice i lineart): PhantasticAnanas
Ilustracje (kolorowanie): Kadan

Publikacja: Kara, Hikari

--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Pchełka
Wersja NanoKarrin: http://nanokarrin.pl/god-eater

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: God Eater - Over the Clouds
Więcej o projekcie: http://nanokarrin.pl/god-eater
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Twitter: http://twitter.com/nanokarrin
//Instagram: NanoKarrin #nanokarrin
//Mail:  info@nanokarrin.pl

--

DISCLAMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the anime and its rightful owners! All Rights Reserved.