Polski fandub Oruchuban Ebichu - Ebichu’s love song 

Słowo od reżysera:
Wesołego Dnia Chomika 2020! Wracamy z sepleniącą, sprośną Ebichu, która zdecydowanie nie powinna bawić się w śpiewanie. Sugerujemy dyskrecję podczas oglądania lub zasłonięcie uszek dewotkom. Rumieńcie się radośnie!

--

EKIPA PROJEKTU: 
Reżyseria, edycja obrazu i tekst piosenki: Grydzia
Korekta: Pchełka
Realizacja dźwięku: Spidek, Shiguroya
Chórki: Shiguroya, Yu, KanadeQ, Klara
Udźwiękowienie: benk86

Wystąpili:
Ebichu - Grydzia
Pani - Rionka
Chłopak - Cearme


Tekst:
Zamek, co w dżungli tkwi
Mój przyciąga wzrok
Gdy zbliżam się
Mówisz mi, że
Musi zapaść zmrok

Lecz ja wiem
Tego chcę
W zamku księżna twa
By prężny król
Z całych swych sił
Zdobył każdą z bram

(Ty łaj-da-ku)
(U-bós-twiam-cię)

Głowa znów boli cię
To przewrotna gra
Nie mów mi kłamstw
Bo w zamku twym
Wielka powódź trwa


--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Grydzia
Wersja NanoKarrin: www.youtube.com/fandubbing

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: Oruchuban Ebichu - Ebichu’s Love Song
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Instagram: #nanokarrin

--

DISCLAIMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the rightful owners!