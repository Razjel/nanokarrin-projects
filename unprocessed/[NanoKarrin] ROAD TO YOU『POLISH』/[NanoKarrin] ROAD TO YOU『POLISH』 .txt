Polski fandub ROAD TO YOU.

--

EKIPA PROJEKTU: 
Reżyseria: Pawlik
Udźwiękowienie i montaż: Pawlik
Scenariusz: Oku

OBSADA:
Yuki - Black Cat
Mai - Aloes
Kei - Mad Neko
Ryouta - Szibin
Toshirou - mru
Jin - Anonimowy Lektor
Szef - Pawlik


PIOSENKA:
Reżyseria: Orzecho
Instrumental: dzyogas
Wokal: Elven
Tekst: Cruci
Korekta tekstu: Pchełka

Publikacja: KaraKopiara

--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Cruci
Wersja NanoKarrin: http://nanokarrin.pl/

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: https://www.youtube.com/watch?v=gLuZhjvAG8M&feature=youtu.be
Więcej o projekcie: http://nanokarrin.pl/
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Instagram: #nanokarrin

--

DISCLAIMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the anime and its rightful owners!