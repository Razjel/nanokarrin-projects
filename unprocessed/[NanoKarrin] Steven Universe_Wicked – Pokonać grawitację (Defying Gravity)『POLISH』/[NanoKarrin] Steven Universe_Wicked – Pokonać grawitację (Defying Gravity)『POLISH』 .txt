Polski fandub/cover piosenki Pokonać grawitację (Defying Gravity) z musicalu Wicked.

Słowo od reżysera:
Dziękuję JankyBones za możliwość wykorzystania i pokolorowania jej fenomenalnego animatica.
Kanał JankyBones: https://www.youtube.com/user/BoneHatter
Oryginalny animatic: https://www.youtube.com/watch?v=s93WClpkRKc

--

EKIPA PROJEKTU: 
Reżyseria: WormyApple
Tekst piosenki: Dorota Kozielska
https://www.youtube.com/watch?v=JqA7mJ-Isf0
Realizacja dźwięku: Memento (mix, master), Wormy (timing, tuning), Hikari (tuning)
Ilustracje: JankyBones (szkice), WormyApple (tła, kolorowanie)
Wideo: JankyBones, WormyApple
Główne wokale: Yuyechka, Hikari
Chórki: Aga, Miyu, Roronoa, WormyApple, snuffk
Kompozycja harmonii: Aga
Pomoc: Hikari, Razjel
Publikacja: WormyApple

--

Tekst: 
To co? Lecimy?

Życzę ci szczęścia…
Tu mogłaś znaleźć je

Ja tobie też
skoro tu zostać chcesz

Miej czego zawsze chciałaś,
obyś nie pożałowała
Szczęśliwa przyjaciółko bądź!
Niech zawsze sprzyja ci los

Jeśli mnie chcecie złapać,
łapcie zachodni wiatr
Ktoś mi niedawno mówił:
każdy prawo ma się wzbić choć raz
Choć lecę dziś w samotność,
przynajmniej wolność mam
Bez podłych ludzi
dużo lepszy będzie świat
Jak feniks co
z popiołów rodzi się
Na przekór im
spadając wznoszę się
Wkrótce o mnie usłyszą znów
Nie patrzę w dół, nie patrzę wstecz
i mogę wreszcie być kim chcę
Nie powstrzymacie już nigdy mnie

Niebieska Perła:
Miej czego chciałaś

Łapać ją! To zdrajca!
Brać ją!

Nigdy już

Łapać ją! To zdrajca!
Nie pozwólcie uciec jej!

--

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: Wicked
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

Wersja Studia Accantus: https://www.youtube.com/watch?v=JqA7mJ-Isf0

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Instagram: #nanokarrin

--

DISCLAIMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the rightful owners!