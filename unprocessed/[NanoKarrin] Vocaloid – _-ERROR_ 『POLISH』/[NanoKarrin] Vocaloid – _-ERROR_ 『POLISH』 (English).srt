1
00:00:25,039 --> 00:00:26,939
The tears falling from my eyes

2
00:00:26,940 --> 00:00:28,500
They blur the world around (me)

3
00:00:28,719 --> 00:00:31,099
I feel as if I couldn't see anything

4
00:00:31,480 --> 00:00:35,480
Is the future convited to the colour of tears?

5
00:00:35,560 --> 00:00:39,100
Is there, still, a chance for hapiness ?

6
00:00:39,100 --> 00:00:41,020
Today everything around (me) is black

7
00:00:41,079 --> 00:00:43,000
Once you were my light

8
00:00:43,000 --> 00:00:45,619
You were giving colours to this world

9
00:00:45,840 --> 00:00:49,859
Now there's darkness. It's dark here again.

10
00:00:50,020 --> 00:00:53,359
I can't hear your voice anymore

11
00:00:53,500 --> 00:00:57,219
Will I ever see you again ?

12
00:00:57,539 --> 00:01:00,859
Am I still hurt?

13
00:01:01,020 --> 00:01:04,359
Why does it hurt me still?

14
00:01:04,640 --> 00:01:11,379
I don't know, however, I will not give up, I will not

15
00:01:11,799 --> 00:01:14,719
Because I believe I will see once again

16
00:01:15,120 --> 00:01:18,420
Just like in my beautiful dream

17
00:01:18,879 --> 00:01:21,979
But when I want to call you

18
00:01:22,239 --> 00:01:25,799
Error

