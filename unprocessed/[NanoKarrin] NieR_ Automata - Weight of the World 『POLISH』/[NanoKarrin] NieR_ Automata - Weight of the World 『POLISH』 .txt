Polski cover Weight of the World z gry NieR: Automata
Debiut reżyserski!

Słowo od reżysera:
Chciałbym bardzo podziękować calutkiej ekipie. Jestem niezwykle szczęśliwy, że miałem zaszczyt pracować właśnie z Wami przy moim pierwszym, pełnoprawnym projekcie!
Mamy nadzieję, że projekt spodoba się Wam tak samo jak nam! :3

--

EKIPA PROJEKTU: 
Reżyseria: Kito
Tekst piosenki: IkiW
Realizacja dźwięku: Spidek
Edycja obrazu: Kapsel
Wokal: LadyCherryZ
Pomoc : Seraskus

--
Tekst:
Zgasło w moim sercu coś
Co nadzieją ludzie zwą
Niebo ma wiele złowieszczych barw
Kiedy maszyn trzasku brak, 
Cisza tłumi rzewny płacz 
I nikt dostrzec go nie ma jak
Boże nasz, czy podnosisz swój bat
Czy przyszło mi zapłacić dziś za każdy błąd? 
Pieśń ta odkupić je ma
Odpowiedz, jeśli słyszysz mój głos
Ześlij jakiś znak

Podniosę ku niebu krzyk
Choć słowom mym pewnie sensu brak
Na swych barkach dźwigam cały ten ból
Być może pewnego dnia
Ochronić dam radę każde z nas
Pozostałam, by sprzeciwić się złu
Ufam, że za jakiś czas zdobędę sił dość, by móc

Chronić świat

Darmo szukać szczęścia nam
Życie utraciło smak
Mimo to czuję we włosach wiatr
Bezsensowny jest mój trud
Grzech przytłacza każdy bunt
A świat nie chce mi szansy dać

Boże nasz, czy podnosisz swój bat?
Czy przyszło mi zapłacić dziś za  każdy błąd?
Pieśń ta odkupić je ma
Odpowiedz, jeśli słyszysz mój głos
Ześlij jakiś znak

Podniosę ku niebu krzyk
Choć słowom mym pewnie sensu brak
Na swych barkach dźwigam cały ten ból
Być może pewnego dnia
Ochronić dam radę każde z nas
Pozostałam, by sprzeciwić się złu
Ufam, że za jakiś czas zdobędę sił dość, by móc

Chronić świat

Podniosę ku niebu krzyk
Choć słowom mym pewnie sensu brak
Na swych barkach dźwigam cały ten ból
Być może pewnego dnia
Ochronić dam radę każde z nas
Pozostałam, by sprzeciwić się złu

Podniosę ku niebu krzyk
Choć słowom mym pewnie sensu brak
Na swych barkach dźwigam cały ten ból
Być może pewnego dnia
Ochronić dam radę każde z nas
Pozostałam, by sprzeciwić się złu
Ufam, że za jakiś czas zdobędę sił dość, by móc

Chronić świat


--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: IkiW
Wersja NanoKarrin: www.youtube.com/fandubbing

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: (NieR: Automata)
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Instagram: #nanokarrin

--

DISCLAIMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the rightful owners!