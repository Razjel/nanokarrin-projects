Polski cover piosenki City of Stars z musicalu La La Land.

A Pchełka na to…
City of Stars było swego czasu jedną z popularniejszych piosenek z La La Landu (w końcu zdobyli Oscara!). Powstało wiele coverów, mash-upów, nowych aranżacji, muzyki czerpiącej z musicalu, ale nie do końca będącej jego odzwierciedleniem. Przed Wami nasze spojrzenie na tę piosenkę. Nie oddaliśmy oryginału jeden do jednego, ale też nie to było naszym celem. Dodaliśmy za to odrobinę siebie. Posłuchajcie.
--

EKIPA PROJEKTU: 
Podkład: MixonPianoPlayer (pianino) i Orzecho (gitara)
Reżyseria: Hikari i Pchełka
Wokal: Divea i Furman (gościnnie)
Tekst: Kat
Korekta tekstu: Pchełka, Hikari, Mirabelle
Realizacja dźwięku: Orzecho
Wideo: Dani
Obróbka graficzna: Kadan
Animacja: Emilek
Ilustracja: Hyuna Lee (http://lee2419.tumblr.com/)
Publikacja: KaraKopiara, Hikari 

--


❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Kat
Wersja NanoKarrin: http://nanokarrin.pl/lalaland-cos

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: La La Land
Tytuł: City of Stars
Więcej o projekcie: http://nanokarrin.pl/lalaland-cos
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2
--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal:          http://nanokarrin.pl
//Discord:       http://discord.gg/nanokarrin
//Facebook:   http://fb.com/nanokarrin
//Twitter:        http://twitter.com/nanokarrin
//Instagram:   NanoKarrin #nanokarrin
//Mail:             info@nanokarrin.pl


--
DISCLAMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the anime and its rightful owners! All Rights Reserved.