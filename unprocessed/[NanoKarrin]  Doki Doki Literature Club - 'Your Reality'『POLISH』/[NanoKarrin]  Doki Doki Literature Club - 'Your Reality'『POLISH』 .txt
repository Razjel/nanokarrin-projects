Polski cover Doki Doki Literature Club - Your Reality (Twoja rzeczywistość).

Słowo od reżysera:
Just Monika?

--

EKIPA PROJEKTU: 
Reżyseria: Spidek
Tekst piosenki: Goyaa
Realizacja dźwięku: Spidek
Edycja obrazu: Kara
Wokal: Taiga
Tuning: Taiga
Publikacja: Kara, Spidek, BartoszKonkol, Dora

--


❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Goyaa
Wersja NanoKarrin: https://nanokarrin.pl/your-reality/

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: Doki Doki Literature Club
Tytuł: Your Reality
Więcej o projekcie: https://nanokarrin.pl/your-reality/
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2
--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal:          http://nanokarrin.pl
//Discord:       http://discord.gg/nanokarrin
//Facebook:   http://fb.com/nanokarrin
//Twitter:        http://twitter.com/nanokarrin
//Instagram:   NanoKarrin #nanokarrin
//Mail:             info@nanokarrin.pl


--
DISCLAMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the anime and its rightful owners! All Rights Reserved.