Polski cover piosenki zespołu Dreamcatcher - Good Night

//Bitwy Dubbingowe to rodzaj turnieju, gdzie pięcioosobowe drużyny mierzą się ze sobą, tworząc piosenki lub scenki do narzuconych z góry tematów.
--

EKIPA PROJEKTU: 
Reżyseria: #SaltyCrackers
Tekst piosenki: Rose
Realizacja dźwięku: Budyś, Kara, Avlönskt 
Edycja obrazu: Kara
Ilustracje: Lizz (szkic, lineart), Kara (szkic, coloring)
Wokal: Kara, Budyś, Rose, Avlönskt, Lizz
Harmonie: Avlönskt (guide, wokal), Budyś, Rose, Kara (wokal)

Publikacja: Kara, Hikari, Budyś

--

❏ Tekst możesz wykorzystać tylko wtedy, gdy zamieścisz poniższą formułkę:
Autor tekstu: Rose
Wersja NanoKarrin: http://nanokarrin.pl/Good-Night

⬇︎Pobierz:
http://nanokarrin.pl/mp3

--

Źródło: https://www.youtube.com/watch?v=Lxfl8LRab_I
Więcej o projekcie: http://nanokarrin.pl/Good-Night
Dodaj angielskie napisy: http://www.youtube.com/timedtext_cs_panel?c=UCue2Utve3Cz8Cb2eIJzWGUQ&tab=2

--

Dziękujemy wszystkim, którzy subskrybują nasz kanał, udostępniają i komentują nasze projekty!

//Portal: http://nanokarrin.pl
//Discord: http://discord.gg/nanokarrin
//Facebook: http://fb.com/nanokarrin
//Twitter: http://twitter.com/nanokarrin
//Instagram: NanoKarrin #nanokarrin
//Mail:  info@nanokarrin.pl

--

DISCLAMER:
Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use.
THIS VIDEO IS FANMADE. WE DO NOT OWN ANYTHING EXCEPT OUR VOICES. NO COPYRIGHT INFRINGEMENT INTENDED.

We do not claim ownership over this original copyrighted material. We don't make any money off this. This is by fans for fans. No copyright infringement intended! Please support the anime and its rightful owners! All Rights Reserved.